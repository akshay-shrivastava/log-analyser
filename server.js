const jsonWebToken = require('jsonwebtoken');
const minimist = require('minimist');

const dotenv = require('./dotenv');
let args = minimist(process.argv.slice(2), {
    alias: {
        h: "help"
    },
});

if (args.h) {
    console.log(`Options:\n
    \t-r: set the root dir where to lock for the .env file\n 
    `);
    return 1;
}

const root = args.r ? args.r.toString() : path.join(__dirname, "../");
dotenv({ root });

const { initSchema } = require('./schema');
const { packResponse } = require('./utils')
const { errorCodes } = require('./errorCodes')
const { getLogger } = require('./logger');
const log = getLogger('log-analyser --> main -->');
//require('./redisIECOClient')

const fastifyLogFile = process.env.LOG_ANALYSER_FASTIFY_LOG_FILE
    ? process.env.LOG_ANALYSER_LOG_FILE
    : '/tmp/logs';

const fastify = require('fastify')({
    logger: {
      level: 'info',
      file: fastifyLogFile
    },
    bodyLimit: 10485760
})

fastify.register(require('fastify-cors'), {
  origin: true
})
fastify.register(require('fastify-formbody'))

// fastify.decorate('backendAuth', async(request, reply) => {
//   try {
//     const { authorization } = request.headers
//     if (authorization === undefined) {
//       reply.code(401).send(packResponse('FAIL', null, '001 Auth token not found'))
//       return
//     }
//     const splitAuth = authorization.split(' ')
//     if(splitAuth.length < 2) {
//       reply.code(401).send(packResponse('FAIL', null, '002 Auth token not found'))
//       return
//     }
//     let ret = await jsonWebToken.verify(
//       splitAuth[1],
//       process.env.LOG_ANALYSER_BACK_JWT_PUB_KEY.replace(/\\n/g, "\n")
//     )
//     ret.token = splitAuth[1]
//     request.jwt = ret
//     return
//   } catch (error) {
//     log.error("backendAuth::error -->", error, request.headers, request.url)
//     reply.code(500).send()
//   }
// })

// fastify.decorate('authenticate', async (request, reply) => {
//   try {
//     const { authorization } = request.headers
//     if (authorization === undefined) {
//         reply.code(401).send(packResponse('FAIL', null, 'auth token not found'))
//         return
//     }
//     const splitAuth = authorization.split(' ');
//     if (splitAuth.length < 2) {
//         reply.code(401).send(packResponse('FAIL', null, 'auth token not found'));
//         return;
//     }
//     const PUB_KEY = process.env['LOG_ANALYSER_JWT_PUB_KEY_' + process.env.LOG_ANALYSER_ENV]
//     let ret = await jsonWebToken.verify(
//       splitAuth[1],
//       PUB_KEY.replace(/\\n/g, "\n")
//     )
//     ret.token = splitAuth[1]
//     request.jwt = ret
//   } catch (error) {
//     const errMsg = {
//       code: errorCodes.ssoJwtVerifyError
//     }
//     reply.send(packResponse('FAIL', null, errMsg))
//   }
// })

fastify.get('/', (request, reply) => {
  return { status: 'up and running' }
})

initSchema(fastify)
const routes = require('./routes');
fastify.register(routes)

fastify.ready(() => {
  log.debug(fastify.printRoutes({ commonPrefix: false }))
})

const startServer = async () => {
  try {
    await fastify.listen(process.env.LOG_ANALYSER_PORT)
    fastify.log.info(`Server listening on port ${fastify.server.address().port}`)
  } catch (error) {
    fastify.log.error(error)
    process.exit(1)
  }
}

startServer()
