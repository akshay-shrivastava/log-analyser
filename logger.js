const LevelError = 'error';
const LevelDebug = 'debug';
const LevelTodo = 'todo';

const debug = require('debug');
const logDebug = require('debug')('ieco::debug --> ');
const logError = require('debug')('ieco::error --> ');
const logTodo = require('debug')('ieco::todo --> ');

class LoggerClass {
    constructor(namespace, logLevel) {
        this.namespace = namespace;
        this.logLevel = logLevel;

        if (logLevel === LevelError) {
            debug.enable('ieco::*');
        }

        if (logLevel === LevelDebug) {
            debug.enable('ieco::debug*,ieco::error*');
        }

        if (logLevel === LevelTodo) {
            debug.enable('ieco::debug*,ieco::error*,ieco::todo*');
        }
    }

    error = (...msg) => { 
        let paramList = [];
        for(let param of msg){
            if (param instanceof Error){
                paramList.push(param.message);
                paramList.push(param.stack);
            } else {
                paramList.push(param)
            }
        }    
        logError(paramList.join('\n')); 
    };
    debug = (...msg) => { logDebug(msg.join(' ')); };
    todo = (...msg) => { logTodo(msg); };
}

const getLogger = (namespace) => {
    return new LoggerClass(namespace, process.env.LOG_ANALYSER_LOG_LEVEL);
};

module.exports = {
    getLogger,
};