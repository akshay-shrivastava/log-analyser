let error = require('debug')('Ms::dotenv::error::');
let debug = require('debug')('Ms::dotenv::debug::');
const fs = require('fs');
const path = require('path');

const _dotenv = (options) => {
    let envPath = process.env[options?.env]
        ? path.join(options.root, `.${process.env[options?.env]}.env`)
        : path.join(options.root, ".env");

    if (fs.existsSync(envPath)) {
        data = fs.readFileSync(envPath, "utf-8");
        data = data.split('\n');

        let cfg = {};

        for (let i in data) {
            let line = data[i];

            //remove comments from line
            if (line.trim().startsWith('#')){
                continue;
            }
            
            let lineSplit = line.split(' #'); //empty space must be before # to diffrentiate value and comment
            let code = lineSplit[0];

            //skip empty line
            if (code.length === 0) {
                continue;
            }

            //sperate keys and values and put it in object
            let keyValue = code.split('=');

              
            if (keyValue.length > 2) {//there have been = in the value, so we need to combine all value with index > 0
                let newValue = keyValue.slice(1).join('=');
                
                keyValue = [
                    keyValue[0],
                    newValue,
                ]
            }

            if (keyValue.length < 2) {
                error("keyValue -->", keyValue)
                let text = "format error --> " + code;
                error(text);
                throw new Error(text);
            } 
           else {
                cfg[keyValue[0].trim()] = keyValue[1].trim();
               
            }
        }

        //merge our config with environment
        for (key in cfg) {
            if (process.env[key] === undefined) {
                process.env[key] = cfg[key];
            }

        }
    }
}

module.exports = _dotenv;