const schema = {
    uploadLogs: {
        $id: 'uploadLogs',
        type: 'object',
        properties: {
            data: { type: 'array' }
        },
        required: ['data']
    }
}

const initSchema = (fastify) => {
    for(let key in schema) {
        fastify.addSchema[schema[key]]
    }
}

module.exports = {
    initSchema
}