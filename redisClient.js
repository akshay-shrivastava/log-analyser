const { promisify } = require('util')
const redis = require('redis')

const client = redis.createClient({
    password: process.env.LOG_ANALYSER_REDIS_PASS
})
const pushLogsInRedisAsync = promisify(client.rpush).bind(client)

const pushLogs = async (logs) => {
    pushLogsInRedisAsync('logQueue', JSON.stringify(logs))
}

module.exports = {
    pushLogs
}