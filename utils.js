
//Used to terminate a request and send response to user
const packResponse = (status, payload, message) => {
    try {
      return { status, payload, message };
    } catch (error) {
      log.error('error in packResponse-->', error);
    }
  };  

module.exports = {
    packResponse
}