const redis = require('redis');

const client = redis.createClient({
  url: process.env.LOG_ANALYSER_IECO_REDIS_URL
});

function delay(delayInms) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(2);
      }, delayInms);
    });
  }

(function pullQueue() {
	console.log(1)
    try {
      client.LPOP('isha::logQueue', async function(err, queueObj) {
        if(err) {
          console.log(`isha::ssoIdQueue, Error while reading content from Redis Queue ${err}`)
        }
        console.log(`isha::ssoIdQueue, Queue Object ${queueObj}`)
        queueObj= JSON.parse(queueObj)
        if(!queueObj){
            await delay(3000);
            pullQueue()
            return
        }
        pullQueue()
      }) 
    } catch (err) {
      console.error(`Error:: ${err}`)
    }
  })()
