const { pushLogs } = require('./redisClient')
const { packResponse } = require('./utils')
const { getLogger } = require('./logger');
const log = getLogger('routes');

const uploadLogs = (request, reply) => {
    try {
        console.log('upload logs', request.body.data)
        pushLogs(request.body.data)
        return packResponse('SUCCESS', null, 'Uploaded logs')
    } catch (error) {
        log.error('uploadLogs -->', error)
        return packResponse('FAIL', null, error.message)
    }
}

const routes = async (fastify,opt,done) => {
    try {
        const routes = {
            uploadLogs: {
                method: 'POST',
                url: '/uploadLogs',
                schema: { body: { $ref: "uploadLogs#" } },
                preValidation: fastify.authenticate,
                handler: uploadLogs
            }
        }
        for (let key in routes) {
            fastify.route(routes[key])
        }
	done()
    } catch (error) {
        log.error('Routes -->', error)        
    }
}

module.exports = routes
